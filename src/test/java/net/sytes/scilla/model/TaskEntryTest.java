package net.sytes.scilla.model;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class TaskEntryTest {

    @org.junit.jupiter.api.Test
    void testToString() {
        TaskEntry te = new TaskEntry();
        te.setId(1);
        te.setName("tname");
        te.setUrgency(TaskEntry.TaskPriority.HIGH);
        te.setStartDate(LocalDateTime.now());
        te.setDescription("tdesc");

        assertEquals(String.class, te.toString().getClass());
    }
}