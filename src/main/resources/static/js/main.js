var inputsDate = ['startDate'];
var editTaskBtns = $('.show-task-form')
var removeTaskBtns = $('.remove-task')

$.fn.refresh = function() {
    var elems = $(this.selector);
    this.splice(0, this.length);
    this.push.apply( this, elems );
    return this;
};

function appendTask(data) {
  let row = $('<tr>', { 'data-id': data.id })
    .appendTo('#task-list tbody');

  let attrsDate = ""
  for (let i in inputsDate) {
    attrsDate = attrsDate.concat('<span class="')
      .concat(inputsDate[i]).concat('">')
      .concat(data[inputsDate[i]] != null ?
        data[inputsDate[i]].replace('T', ' ') :
        'NIL'
      )
      .concat('</span>');
  }

  let attrs = [
    '<a class="name" href="/tasks/' + data.id + '">' + data.name + '</a> ' +
    '<a href="#" class="tooltip">?' +
    '<span class="tooltiptext description">' + data.description + '</span></a>',
    attrsDate,
    '<span class="urgency">' + data.urgency + '</span>',
  ]
  for (let i in attrs) {
    row.append('<td>'+attrs[i]+'</td>')
  }

  $.merge(
    $('<button>', {
      text: 'Правка',
      class: 'show-task-form'
    }),
    $('<button>', {
      text: 'Удалить',
      class: 'remove-task'
    })
  ).wrapAll($('<td>', {class: 'task-button'})).parent().appendTo(row);

  $('h1 span').text(+$('h1 span').text()+1)

  if ($('#task-list').css('visibility') != 'visible')
    $('#task-list').css('visibility', 'visible');
}

$('#task-list').on('click', '.show-task-form', function() {
  let form = $('#task-form');
  let taskID = $(this).parent().parent().data('id');
  let inputs = $(form).find('[name]');
  $.ajax({
    method: 'GET',
    url: '/tasks/' + taskID,
    success: (r) => {
      for (let i = 0; i < inputs.length; i++) {
        inputs[i].value = r[inputs[i].getAttribute('name')];
      }
    }
  })

  let shadow = $('<div>', { id: 'shadow'});
  shadow.appendTo('body');

  form.data('action', 'update');
  form.find('h2').text('Правка');
  form.find('#save-task').data('id', taskID);
  form.css('display', 'inline-flex');

  shadow.click(function() {
    $('#shadow').remove();
    form.css('display', 'none');
    form.data('action', 'add');
    form.find('h2').text('Добавление');
    for (let i = 0; i < inputs.length; i++) {
      inputs[i].value = '';
    }
  });
  return false;
});

$('#task-list').on('click', '.remove-task',function() {
  let taskID = $(this).parent().parent().data('id');
  $.ajax({
    method: 'DELETE',
    url: '/tasks/' + taskID,
    success: (r) => {
      $('[data-id="' + taskID + '"]').remove();
      $('h1 span').text(+$('h1 span').text()-1)
      if ($('h1 span').text() == "0")
        $('#task-list').css('visibility', 'collapse');
    }
  });
  return false;
});

// display task adding form
$('#add-task').click(function() {
  let form = $('#task-form');
  let shadow = $('<div>', { id: 'shadow'});
  shadow.appendTo('body');
  form.css('display', 'inline-flex');
  shadow.click(function() {
      $('#shadow').remove();
      form.css('display', 'none');
  });
});

// saving a task
$('#save-task').click(function() {
  let form = $('#task-form');
  let data = serializeForm(form.find('form'));

  let method = 'POST';
  let url = '/tasks/';
  let taskID;

  if (form.data('action') == 'update') {
    method = 'PUT';
    taskID = $(this).data('id');
    url = '/tasks/' + taskID;
    let keys = Object.keys(data);
    for (i in keys) {
      let currentAttr = (inputsDate.includes(keys[i])) ?
        $('[data-id="' + taskID + '"]').find('.' + keys[i]).text()
          .replace(' ', 'T'):
        $('[data-id="' + taskID + '"]').find('.' + keys[i]).text();
      if (currentAttr.localeCompare(data[keys[i]]) == 0) {
        delete data[keys[i]];
      }
    }
    if (keys.length != Object.keys(data).length) {
      method = 'PATCH';
    }
  }

  $.ajax({
    method: method,
    url: url,
    dataType: 'json',
    contentType: 'application/json;charset=UTF-8',
    data: JSON.stringify(data),
    success: (r) => {
      // hide form, change its action to `add`
      form.css('display', 'none');
      form.data('action', 'add');
      $('#shadow').remove();
      let task = {};
      for (i in data) {
        task[i] = data[i];
      }
      if (method == 'POST') {
        task.id = r;
        appendTask(task);
      } else {
        for (i in task) {
          let attr = (inputsDate.includes(i)) ?
            task[i].replace('T', ' ') : task[i];
          $('[data-id="' + taskID + '"]').find('.' + i).text(attr);
        }
      }
      form.find('input,textarea').val("")
    }
  });
  return false;
});

function handleResponse(r) {

}

function serializeForm(f) {
  let r = {}
  f.serializeArray().forEach(
    e => { Object.assign(r, { [e['name']]: e['value'].trim() }) }
  );
  return r;
}

//$.get('/tasks/', function(r) {
//  for (i in r) {
//    appendTask(r[i]);
//  }
//});

$('[name=description]').attr('placeholder', 'Краткое описание').addClass('descr');
$('input[type="datetime-local"]').attr('max', '9999-12-31T23:59:59');
