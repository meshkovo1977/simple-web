package net.sytes.scilla.repo;

import net.sytes.scilla.model.TaskEntry;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepository extends CrudRepository<TaskEntry, Integer> {
}
