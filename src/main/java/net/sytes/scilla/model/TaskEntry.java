package net.sytes.scilla.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Entity
@Table(name = "Tasks")
public class TaskEntry {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @NotBlank(message = "is required")
    @Size(max = 32, message = "is too large")
    private String name;

    @NotNull(message = "make a decision when to start")
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime startDate;

    private TaskPriority urgency;

    // just to internalize
    @Size(max = 512, message = "is too large")
    private String description;

    public enum TaskPriority {
        LOW,
        MEDIUM,
        HIGH
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public TaskPriority getUrgency() {
        return urgency;
    }

    public void setUrgency(TaskPriority urgency) {
        this.urgency = urgency;
    }

    @Override
    public String toString() {
        return "TaskEntry{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", startDate=" + startDate +
            ", priority=" + urgency +
            '}';
    }
}
