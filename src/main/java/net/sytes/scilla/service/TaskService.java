package net.sytes.scilla.service;

import net.sytes.scilla.model.TaskEntry;

import java.util.List;
import java.util.Map;

public interface TaskService {
    List<TaskEntry> getAllTasks();
    TaskEntry getTask(int id);
    int addTask(TaskEntry task);
    TaskEntry updateTask(int id, TaskEntry task);
    TaskEntry partialUpdateTask(int id, Map<String, Object> updates);
    TaskEntry deleteTask(int id);
}
