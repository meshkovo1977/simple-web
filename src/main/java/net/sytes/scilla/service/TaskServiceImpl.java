package net.sytes.scilla.service;

import net.sytes.scilla.model.TaskEntry;
import net.sytes.scilla.repo.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class TaskServiceImpl implements TaskService {
    private static DateTimeFormatter f = DateTimeFormatter
        .ofPattern("yyyy-MM-dd'T'HH:mm");

    @Autowired
    private TaskRepository taskRepository;

    @Override
    public List<TaskEntry> getAllTasks() {
        List<TaskEntry> l = new ArrayList<>();
        taskRepository.findAll().forEach(l::add);
        return l;
    }

    @Override
    public TaskEntry getTask(int id) {
        Optional<TaskEntry> ot = taskRepository.findById(id);
        return ot.orElse(null);
    }

    @Override
    public int addTask(TaskEntry task) {
        return taskRepository.save(task).getId();
    }

    @Override
    public TaskEntry updateTask(int id, TaskEntry task) {
        Optional<TaskEntry> ot = taskRepository.findById(id);
        if (ot.isPresent()) {
            TaskEntry t = ot.get();
            t.setName(task.getName());
            t.setStartDate(task.getStartDate());
            t.setUrgency(task.getUrgency());
            t.setDescription(task.getDescription());
            return taskRepository.save(t);
        }
        return null;
    }

    @Override
    public TaskEntry partialUpdateTask(int id, Map<String, Object> updates) {
        TaskEntry t = getTask(id);
        if (t == null)
            return null;
        updates.forEach((k, v) -> {
            switch (k) {
                case ("name"): {
                    t.setName((String) v);
                    break;
                }
                case ("startDate"): {
                    t.setStartDate(LocalDateTime.parse((String) v, f));
                    break;
                }
                case ("description"): {
                    t.setDescription((String) v);
                    break;
                }
                case ("urgency"): {
                    String u = (String) v;
                    if (u.equals("LOW")) t.setUrgency(TaskEntry.TaskPriority.LOW);
                    if (u.equals("MEDIUM")) t.setUrgency(TaskEntry.TaskPriority.MEDIUM);
                    if (u.equals("HIGH")) t.setUrgency(TaskEntry.TaskPriority.HIGH);
                    break;
                }
            }
        });
        return taskRepository.save(t);
    }

    @Override
    public TaskEntry deleteTask(int id) {
        TaskEntry t = getTask(id);
        if (t != null) {
            taskRepository.deleteById(id);
            return t;
        }
        return null;
    }
}
