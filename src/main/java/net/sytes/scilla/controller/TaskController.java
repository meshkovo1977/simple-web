package net.sytes.scilla.controller;

import net.sytes.scilla.model.TaskEntry;
import net.sytes.scilla.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/tasks/")
public class TaskController {
    @Autowired
    private TaskService taskService;

    @GetMapping
    public List<TaskEntry> getAll() {
        Iterable<TaskEntry> taskEntries = taskService.getAllTasks();
        List<TaskEntry> tasks = new ArrayList<>();
        for (TaskEntry t : taskEntries)
            tasks.add(t);
        return tasks;
    }

    @GetMapping("/{id}")
    public ResponseEntity<TaskEntry> getTask(@PathVariable int id) {
        TaskEntry t = taskService.getTask(id);
        if (t == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(t);
    }

    @PostMapping
    public int add(@Valid @RequestBody TaskEntry task) {
        return taskService.addTask(task);
    }

    @PutMapping("/{id}")
    public ResponseEntity update(@Valid @RequestBody TaskEntry newTask,
        @PathVariable int id) {
        TaskEntry t = taskService.updateTask(id, newTask);
        if (t == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(t);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<?> partialUpdate(
        @RequestBody Map<String, Object> updates,
        @PathVariable int id
        ) {
        TaskEntry t = taskService.partialUpdateTask(id, updates);
        if (t == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(t);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable int id) {
        if (taskService.deleteTask(id) == null)
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok().build();
    }
}
