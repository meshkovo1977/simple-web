package net.sytes.scilla.controller;


import net.sytes.scilla.model.TaskEntry;
import net.sytes.scilla.repo.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;

@Controller
public class DefaultController {

    @Autowired
    private TaskRepository taskRepository;

    @Value("${customParam}")
    private String customParam;

    @RequestMapping("/")
    public String index(Model model) {
        Iterable<TaskEntry> taskEntries = taskRepository.findAll();
        ArrayList<TaskEntry> tasks = new ArrayList<>();
        for (TaskEntry t: taskEntries) {
            tasks.add(t);
        }
        model.addAttribute("tasks", tasks);
        model.addAttribute("tasksCount", tasks.size());
        model.addAttribute("customParam", customParam);
        return "index";
    }
}
